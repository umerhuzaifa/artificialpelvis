function [D] = fcn_D(q,params)

D = zeros(7,7);

  D(1,1)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          (params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 + (params(9)^2*params(2))/4 +...
          (params(10)^2*params(3))/4 + (params(8)^2*params(1))/4 + params(11)*params(12)*params(4)*cos(q(6)) + params(13)*...
         params(14)*params(7)*cos(q(7));
  D(1,2)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          (params(10)^2*params(3))/4 + params(11)*params(12)*params(4)*cos(q(6));
  D(1,3)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          (params(9)^2*params(2))/4 + params(13)*params(14)*params(7)*cos(q(7));
  D(1,4)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          params(11)*params(12)*params(4)*cos(q(6));
  D(1,5)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          params(13)*params(14)*params(7)*cos(q(7));
  D(1,6)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(1,7)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(2,1)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          (params(10)^2*params(3))/4 + params(11)*params(12)*params(4)*cos(q(6));
  D(2,2)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          (params(10)^2*params(3))/4 + params(11)*params(12)*params(4)*cos(q(6));
  D(2,3)=0;
  D(2,4)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          params(11)*params(12)*params(4)*cos(q(6));
  D(2,5)=0;
  D(2,6)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(2,7)=0;
  D(3,1)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          (params(9)^2*params(2))/4 + params(13)*params(14)*params(7)*cos(q(7));
  D(3,2)=0;
  D(3,3)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          (params(9)^2*params(2))/4 + params(13)*params(14)*params(7)*cos(q(7));
  D(3,4)=0;
  D(3,5)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          params(13)*params(14)*params(7)*cos(q(7));
  D(3,6)=0;
  D(3,7)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(4,1)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          params(11)*params(12)*params(4)*cos(q(6));
  D(4,2)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          params(11)*params(12)*params(4)*cos(q(6));
  D(4,3)=0;
  D(4,4)=(params(11)^2*params(4))/4 + params(12)^2*params(4) + (params(12)^2*params(5))/4 +...
          params(11)*params(12)*params(4)*cos(q(6));
  D(4,5)=0;
  D(4,6)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(4,7)=0;
  D(5,1)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          params(13)*params(14)*params(7)*cos(q(7));
  D(5,2)=0;
  D(5,3)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          params(13)*params(14)*params(7)*cos(q(7));
  D(5,4)=0;
  D(5,5)=(params(13)^2*params(6))/4 + params(13)^2*params(7) + (params(14)^2*params(7))/4 +...
          params(13)*params(14)*params(7)*cos(q(7));
  D(5,6)=0;
  D(5,7)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(6,1)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(6,2)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(6,3)=0;
  D(6,4)=(params(11)*params(4)*(params(11) + 2*params(12)*cos(q(6))))/4;
  D(6,5)=0;
  D(6,6)=(params(11)^2*params(4))/4;
  D(6,7)=0;
  D(7,1)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(7,2)=0;
  D(7,3)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(7,4)=0;
  D(7,5)=(params(14)*params(7)*(params(14) + 2*params(13)*cos(q(7))))/4;
  D(7,6)=0;
  D(7,7)=(params(14)^2*params(7))/4;

 