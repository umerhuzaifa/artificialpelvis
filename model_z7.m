function dz = model_z7(t, z, sim_params)
global control;
params = leg_parameters;
tf = 1;

%z = [q; dq];
q = z(1:7);
dq = z(8:14);

z2 = z(8:14);


D = fcn_D(q, params);
C = fcn_C(q, dq, params);
G = fcn_G(q, params);


%%% Sinusoidal function oscillating between 60 and -60 degrees
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


qd_pelvis =  [5 * pi/180;...    % torso
    (90*pi/180-q(1))* sin(t);...  % pelvis with th2
    (-90*pi/180 -q(1))* sin(t)];   % pelvis with th3

dqd_pelvis = [0; 60*pi/180 * cos(t);...
    -60*pi/180 * cos(t)];

Kp_pelvis = 20 * eye(3); Kd_pelvis = 5 * eye(3);


% use of relative angles
v1 = - Kp_pelvis * (q(1:3)- qd_pelvis) ...
    - Kd_pelvis * (dq(1:3) -dqd_pelvis);

% Augmenting the no-control pelvis term
v1 = [v1; zeros(4, 1)];  


qd_legs = [2*pi/3-q(1)-q(2);...  % r_thigh needs to be at absolute angle 120
    pi-q(1)-q(3);...          % l_thigh needs to be at absolute angle 180
    40* pi/180;...          % r_shank needs to be at angle 40
    60* pi/180];   % l_shank needs to be at angle 60

Kp_legs = 20* eye(4); Kd_legs = 5 * eye(4);


% use of relative angles

v2 = -Kp_legs * (q(4:7)- qd_legs) ...
    - Kd_legs * (dq(4:7) - zeros(4,1));
v2 = [zeros(3, 1); v2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Feedforward compensation with oscillating torsos

tau = C*z2 + G + v1 + v2;
control = [control tau];
dz = [z2; inv(D) * (tau - G  - C*z2)]; 
end