function [C] = fcn_C(q,dq,params)

C = zeros(7,7);

  C(1,1)=- (dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(7)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2;
  C(1,2)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(1,3)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(1,4)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(1,5)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(1,6)=- (dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 - (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(6)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2;
  C(1,7)=- (dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 - (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(7)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2;
  C(2,1)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(2,2)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(2,3)=0;
  C(2,4)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(2,5)=0;
  C(2,6)=- (dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 - (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(6)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2;
  C(2,7)=0;
  C(3,1)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(3,2)=0;
  C(3,3)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(3,4)=0;
  C(3,5)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(3,6)=0;
  C(3,7)=- (dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 - (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(7)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2;
  C(4,1)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(4,2)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(4,3)=0;
  C(4,4)=-(dq(6)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(4,5)=0;
  C(4,6)=- (dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 - (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2 - (dq(6)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2;
  C(4,7)=0;
  C(5,1)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(5,2)=0;
  C(5,3)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(5,4)=0;
  C(5,5)=-(dq(7)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(5,6)=0;
  C(5,7)=- (dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 - (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2 - (dq(7)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2;
  C(6,1)=(dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 + (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 + (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(6,2)=(dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 + (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 + (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(6,3)=0;
  C(6,4)=(dq(1)*params(11)*params(12)*params(4)*sin(q(6)))/2 + (dq(2)*params(11)*params(12)*...
         params(4)*sin(q(6)))/2 + (dq(4)*params(11)*params(12)*params(4)*sin(q(6)))/2;
  C(6,5)=0;
  C(6,6)=0;
  C(6,7)=0;
  C(7,1)=(dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 + (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 + (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(7,2)=0;
  C(7,3)=(dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 + (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 + (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(7,4)=0;
  C(7,5)=(dq(1)*params(13)*params(14)*params(7)*sin(q(7)))/2 + (dq(3)*params(13)*params(14)*...
         params(7)*sin(q(7)))/2 + (dq(5)*params(13)*params(14)*params(7)*sin(q(7)))/2;
  C(7,6)=0;
  C(7,7)=0;

 