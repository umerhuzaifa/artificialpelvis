function [c_list m_list] = gen_c_m_lists(vec, str_prefix)
    c_list = {} ; m_list = {} ;
    for j=1:length(vec)
        c_list{j,1} = char(vec(j)) ;
        c_list{j,2} = [str_prefix '(' num2str(j-1) ')'] ;
        m_list{j,1} = char(vec(j)) ;
        m_list{j,2} = [str_prefix '(' num2str(j) ')'] ;
    end
end