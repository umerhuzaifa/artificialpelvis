function [phip,plnt,plnl,plnr,pln2,pln3,pleft_knee,pright_knee,pln1,pln4,pright_foot,pleft_foot] = fcn_linkpos(q,params)

phip = zeros(2,1);

plnt = zeros(2,1);

plnl = zeros(2,1);

plnr = zeros(2,1);

pln2 = zeros(2,1);

pln3 = zeros(2,1);

pleft_knee = zeros(2,1);

pright_knee = zeros(2,1);

pln1 = zeros(2,1);

pln4 = zeros(2,1);

pright_foot = zeros(2,1);

pleft_foot = zeros(2,1);

  phip(1,1)=0;
  phip(2,1)=0;

   plnt(1,1)=(params(8)*sin(q(1)))/2;
  plnt(2,1)=(params(8)*cos(q(1)))/2;

   plnl(1,1)=(params(9)*sin(q(1) + q(3)))/2;
  plnl(2,1)=(params(9)*cos(q(1) + q(3)))/2;

   plnr(1,1)=(params(10)*sin(q(1) + q(2)))/2;
  plnr(2,1)=(params(10)*cos(q(1) + q(2)))/2;

   pln2(1,1)=(params(12)*sin(q(1) + q(2) + q(4)))/2;
  pln2(2,1)=(params(12)*cos(q(1) + q(2) + q(4)))/2;

   pln3(1,1)=(params(13)*sin(q(1) + q(3) + q(5)))/2;
  pln3(2,1)=(params(13)*cos(q(1) + q(3) + q(5)))/2;

   pleft_knee(1,1)=params(13)*sin(q(1) + q(3) + q(5));
  pleft_knee(2,1)=params(13)*cos(q(1) + q(3) + q(5));

   pright_knee(1,1)=params(12)*sin(q(1) + q(2) + q(4));
  pright_knee(2,1)=params(12)*cos(q(1) + q(2) + q(4));

   pln1(1,1)=(params(11)*sin(q(1) + q(2) + q(4) + q(6)))/2 + params(12)*sin(q(1) + q(2) + q(4));
  pln1(2,1)=(params(11)*cos(q(1) + q(2) + q(4) + q(6)))/2 + params(12)*cos(q(1) + q(2) + q(4));

   pln4(1,1)=(params(14)*sin(q(1) + q(3) + q(5) + q(7)))/2 + params(13)*sin(q(1) + q(3) + q(5));
  pln4(2,1)=(params(14)*cos(q(1) + q(3) + q(5) + q(7)))/2 + params(13)*cos(q(1) + q(3) + q(5));

   pright_foot(1,1)=params(11)*sin(q(1) + q(2) + q(4) + q(6)) + params(12)*sin(q(1) + q(2) + q(4));
  pright_foot(2,1)=params(11)*cos(q(1) + q(2) + q(4) + q(6)) + params(12)*cos(q(1) + q(2) + q(4));

   pleft_foot(1,1)=params(14)*sin(q(1) + q(3) + q(5) + q(7)) + params(13)*sin(q(1) + q(3) + q(5));
  pleft_foot(2,1)=params(14)*cos(q(1) + q(3) + q(5) + q(7)) + params(13)*cos(q(1) + q(3) + q(5));

 