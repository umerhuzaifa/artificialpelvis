
% This script loads the data obtained after simulation run in the main
% file, and plots all the useful quantities here.

% Umer Huzaifa
% April 19, 2021

load('sim_data.mat')


figure
% angle vs time plot
set(gcf, 'Position',[1443 303 729 447])
subplot(121)
qt = z(:,1:7);
plot(t, qt(:,1:3), 'LineWidth', 4);
legend('$\theta_1$','$\theta_2$','$\theta_3$','interpreter', 'latex');
title('Joint Angles for the Torso and Pelvis')
set(gca, 'FontSize', 14)

subplot(122)
plot(t, qt(:,4:7), 'LineWidth', 4);
legend('$\theta_4$','$\theta_5$','$\theta_6$','$\theta_7$', 'interpreter', 'latex');
title('Joint Angles for the Legs')

xlabel('time (s)');
ylabel('Angles (rad)');

set(gca, 'FontSize', 14)

set(gcf, 'Color', 'white')

figure
% velocity vs time plot
set(gcf, 'Position',[1444 -228 729 447])
subplot(121)
dqt = z(:,1:7);
plot(t, dqt(:,1:3), 'LineWidth', 4);
legend('$\dot \theta_1$','$\dot \theta_2$','$\dot \theta_3$','interpreter', 'latex');
title('Joint Velocities for the Torso and Pelvis')
set(gca, 'FontSize', 12)

subplot(122)
plot(t, dqt(:,4:7), 'LineWidth', 4);
legend('$\dot \theta_4$','$\dot \theta_5$','$\dot \theta_6$','$\dot \theta_7$', 'interpreter', 'latex');
title('Joint Velocities for the Legs')

xlabel('time (s)');
ylabel('Angles (rad)');

set(gca, 'FontSize', 12)
set(gcf, 'Color', 'white')

figure
% plotting the center of mass

set(gcf, 'Position',[1444 -228 729 447])
q = z(:, 1:7);
[com_x, com_y] = com_model(q);
plot(t, com_x, 'LineWidth', 4)
hold on
plot(t, com_y, 'LineWidth', 4)
set(gca, 'FontSize', 22)
set(gcf, 'Color', 'white')
xlabel('$\textrm{time (s)}$', 'interpreter', 'latex')
ylabel('$\textrm{Center of Mass (m)}$', 'interpreter', 'latex')
legend('$p^x_{com}$', '$p^y_{com}$', 'interpreter', 'latex','FontSize',22)
axis auto

figure(1)
hold on
plot(com_x, com_y, 'r*')


figure

% torque vs time plot
set(gcf, 'Position',[2175 -206 567 426])
subplot(121)
ctrl_tor = control(1:3, :);
plot(ctrl_tor', 'LineWidth', 4);
legend('$\tau_1$','$\tau_2$','$\tau_3$','interpreter', 'latex');
title('Torques Applied by Torso and Pelvic Structures')
set(gca, 'FontSize', 14)

subplot(122)
ctrl_legs = control(4:7, :);
plot(ctrl_legs', 'LineWidth', 4);
legend('$\tau_4$','$\tau_5$','$\tau_6$','$\tau_7$', 'interpreter', 'latex');
title('Torques Applied by the Joint Angles in Legs')

xlabel('time (s)');
ylabel('Torque (Nm)');

set(gca, 'FontSize', 14)

set(gcf, 'Color', 'white')