function params = leg_parameters

% Human body parameters

m_tot = 79.2; % kg
l_tot = 1.785; % m 
% All the masses in the six link model


m1 = 0.1 * m_tot; %0.25; 
m2 = 0.1 * m_tot; %0.25; 
m3 = 0.0465 * m_tot; %0.5;
m4 = 0.0465 * m_tot; %0.5;

mt = m_tot - (m1+m2+m3+m4); %0.8; 
ml = 0.25 * mt; %0.6; 
mr = 0.25 * mt; %0.6; 


% All the link lengths in the six link model
% lnt = 0.8;
% lnl = 0.8;
% lnr = 0.8;
% ln1 = 0.8; 
% ln2 = 0.8; 
% ln3 = 0.8; 
% ln4 = 0.8;

ln1 = 0.245 * l_tot;
ln2 = 0.245 * l_tot;
ln3 = 0.246 * l_tot;
ln4 = 0.246 * l_tot;

lnt = l_tot - (ln1+ln2);

lnl = 0.5 * lnt;
lnr = 0.5 * lnt;




g = 9.8;

params = [mt, ml, mr, m1, m2, m3, m4, lnt, lnl, lnr, ln1, ln2, ln3, ln4, g];