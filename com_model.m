% The following function is for calculating the center of mass position of
% the bipedal model.

% Inputs:  
%           q     - joint angle values
% Outputs:  
%           com_x - horizontal co-ordinate of the center of mass
%           com_y - vertical co-ordinate of the center of mass

% Umer Huzaifa
% April 19, 2021


function [com_x, com_y] = com_model(q)

params = leg_parameters;

com_x = []; com_y = [];
if size(q, 1)>1
    for i=1:size(q, 1)
        com = fcn_com(q(i, :), params);
        com_x = [com_x; com(1)]; com_y = [com_y; com(2)];
    end
    
else
    com = fcn_com(q, params);
    com_x = com(1); com_y = com(2);
end