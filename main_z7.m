% This file plots how each angle [q1...q7] and velocity [dq1...dq7] changes
% within a pendulum system in a given time frame with specified initial 
% conditions for angels

% Ziyan Liu
% April 16, 2021

clear all
close all
clc

dbstop if error

t0 = 0; % initial time for integration
tf = 50; % terminating time for integration


% z0 is providing the initial conditions for the differential equations
z0 = pi/180 * [40; ... torso
    10; ... right pelvis th2
    40; ... left pelvis th3
    100; .... right thigh th4
    90; ... left thigh th5        
    40; ... right shank th6
    60; ... left shank th7
    0 ; 0; 0; 0; 0; 0; 0]; 
sim_params = tf;
global control;
[t, z] = ode45(@model_z7, [t0, tf], z0, sim_params);

save('sim_data.mat');

draw_model

seven_linkmodel_plot

