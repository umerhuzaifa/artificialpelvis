% Script for Modeling the Human with Articulated Pelvis as a 7 Link System.
% 7 Links are as follows:
% 1- Torso link imitating the bare human upper part (lnt, mt, th_1)
% 2- Right side pendulum link (ln_r, mr, th_3)
% 3- Left side pendulum link (ln_l, ml, th_2)
% 4- Left side thigh (ln2, m2, th_4)
% 5- Right side thigh (ln3, m3, th_5)
% 6- Left side shank (ln1, m1, th_6)
% 7- Right side shank (ln4, m4, th_7)
% Ziyan Liu
% April 16, 2021

% Updates:

% - Updated the description
% - Added the definition of the center of mass
% - Removed the 'deg' from all the expressions

% Umer Huzaifa
% April 19, 2021


clear all
close all
clc

dbstop if error

%% Equations of motion for the structure
%%%%%%%%%%%
syms ln1 ln2 ln3 ln4 lnl lnr lnt real
syms theta1 theta2 theta3 theta4 theta5 theta6 theta7 real
syms dtheta1 dtheta2 dtheta3 dtheta4 dtheta5 dtheta6 dtheta7 real
syms ddtheta1 ddtheta2 ddtheta3 ddtheta4 ddtheta5 ddtheta6 ddtheta7 real

syms m1 m2 m3 m4 ml mr mt real
syms g real

% symbolic variables for equations of motion
q = [theta1; theta2; theta3; theta4; theta5; theta6; theta7];
dq = [dtheta1; dtheta2; dtheta3; dtheta4; dtheta5; dtheta6; dtheta7];
ddq = [ddtheta1; ddtheta2; ddtheta3; ddtheta4; ddtheta5; ddtheta6; ddtheta7];

% In the position calculations, we have assumed the positive angles in the
% counter-clockwise direction of rotation

% position of hip
phip = [0;0];

% rot2 = @(th) [cos(th - pi/2) -sin(th - pi/2); sin(th - pi/2) cos(th - pi/2)];

% position of mass for lnt (torso)
plnt = rot2(-theta1) * [0; lnt/2];

% position of mass for lnl (right pendulum)
plnr = rot2(-theta1 - theta2) * [0; lnr/2];

% position of mass for lnr (left pendulum)
plnl = rot2(-theta1 - theta3) * [0; lnl/2];

% position of mass for ln2 (right thigh)
pln2 = rot2(-theta1 - theta2 - theta4) * [0; ln2 / 2];

% position of mass for ln3 (left thigh)
pln3 = rot2(-theta1 - theta3 - theta5) * [0; ln3 / 2];

% position of mass for pright_knee (right knee)
pright_knee = rot2(-theta1 - theta2 - theta4) * [0; ln2];

% position of mass for pright_knee (left knee)
pleft_knee = rot2(-theta1 - theta3 - theta5) * [0; ln3];

% position of mass for ln1 (right shank)
pln1 = pright_knee + rot2(-theta1 - theta2 - theta4 - theta6) * [0; ln1 / 2];

% position of right foot
pright_foot = pright_knee + rot2(-theta1 - theta2 - theta4 - theta6) * [0; ln1];

% position of left foot
pleft_foot = pleft_knee + rot2(-theta1 - theta3 - theta5 -theta7) * [0; ln4];

% position of mass for ln4 (left shank)
pln4 = pleft_knee + rot2(-theta1 - theta3 - theta5 -theta7) * [0; ln4 / 2];


% Code for finding KE
% Squaring a matrix: 1. transpose; 2. times it self
KE_lnt = (1/2) * mt * (jacobian(plnt, q) * dq)' * (jacobian(plnt, q) * dq);
KE_lnl = (1/2) * ml * (jacobian(plnl, q) * dq)' * (jacobian(plnl, q) * dq);
KE_lnr = (1/2) * mr * (jacobian(plnr, q) * dq)' * (jacobian(plnr, q) * dq);
KE_ln1 = (1/2) * m1 * (jacobian(pln1, q) * dq)' * (jacobian(pln1, q) * dq);
KE_ln2 = (1/2) * m2 * (jacobian(pln2, q) * dq)' * (jacobian(pln2, q) * dq);
KE_ln3 = (1/2) * m3 * (jacobian(pln3, q) * dq)' * (jacobian(pln3, q) * dq);
KE_ln4 = (1/2) * m4 * (jacobian(pln4, q) * dq)' * (jacobian(pln4, q) * dq);
% KE1 = (1/2) * m1 * (v^2) = (1/2) * m1 * [(d(p1)/dt)]^2
KE = KE_lnt + KE_lnl+ KE_lnr+ KE_ln1+ KE_ln2 + KE_ln3 + KE_ln4;

% Code for finding PE 
% Only extract the height of the center of masses
% Check how to extract
PE_lnt = mt * g * plnt(2);
PE_lnl = ml * g * plnl(2);
PE_lnr = mr * g * plnr(2);
PE_ln1 = m1 * g * pln1(2); % Accessing the virtual component of the matrix (second element) 
PE_ln2 = m2 * g * pln2(2); % virtical distance to the ground
PE_ln3 = m3 * g * pln3(2);
PE_ln4 = m4 * g * pln4(2);

% PE = (total mass) * g * center of mass
PE = PE_lnt + PE_lnl + PE_lnr + PE_ln1 + PE_ln2 + PE_ln3 + PE_ln4;

% Providing the code for deriving D, C, and G matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


D = simplify(jacobian(jacobian(KE, dq).', dq));

% The nested for loop below is written by Prof. Huzaifa
N=max(size(q));
syms C
for k=1:N
    for j=1:N
        C(k,j)=0*g;
        for i=1:N
            C(k,j)=C(k,j)+1/2*(diff(D(k,j),q(i))+...
                diff(D(k,i),q(j))-...
                diff(D(i,j),q(k)))*dq(i);
        end
    end
end

G=simplify(jacobian(PE,q).');

% Center of mass of the robot is the overall effect of the body's mass
% evaluated on a single point. Movement of this point lends insight about
% how the complete robot system moves

COM = (plnt + plnl + plnr + pln2 + pln3 + pln1 + pln4)/...
    (mt + ml + mr + m1 + m2 + m3 + m4);


% Writing all the system parameters into some special form of vectors
data = {} ;

params = {'mt', mt; ...
          'ml', ml; ...
          'mr', mr; ...
          'm1', m1; ...
          'm2', m2; ...
          'm3', m3; ...
          'm4', m4; ...
     
          'lnt', lnt;...
          'lnl', lnl;...
          'lnr', lnr;...
          'ln1', ln1;...
          'ln2', ln2;...
          'ln3', ln3;...
          'ln4', ln4;...
          
          'g', g};

for j=1:length(params) % Temporarily doesn't know the purpose
    data(j,1) = params(j,1) ;
    data(j,2) = {['params(' num2str(j) ')']} ;
end
m_list_params = data;
%%%%

% mapping functions to relate all the variables to their index positions in
% q, dq, and ddq
[~, m_list_q] = gen_c_m_lists(q, 'q');
[~, m_list_dq] = gen_c_m_lists(dq, 'dq') ;
% probably dont need it 
[~, m_list_ddq] = gen_c_m_lists(ddq, 'ddq') ;
% x here is equivalent to z
[~, m_list_x] = gen_c_m_lists([q; dq], 'x') ;


m_output_dir = pwd;   % makes sure that new functions are stored in the same folder
write_fcn_m([m_output_dir '/fcn_D.m'],{'q', 'params'},[m_list_q; m_list_params],{D,'D'});
write_fcn_m([m_output_dir '/fcn_C.m'],{'q','dq', 'params'},[m_list_q;m_list_dq;m_list_params],{C,'C'});
write_fcn_m([m_output_dir '/fcn_G.m'],{'q', 'params'},[m_list_q;m_list_params],{G,'G'});
write_fcn_m([m_output_dir '/fcn_com.m'],{'q', 'params'},[m_list_q;m_list_params],{COM,'COM'});
write_fcn_m([m_output_dir '/fcn_linkpos.m'],{'q', 'params'},[m_list_q;m_list_params],{phip, 'phip';plnt,'plnt'; plnl, 'plnl';...
    plnr, 'plnr';pln2,'pln2';pln3,'pln3';pleft_knee,'pleft_knee';pright_knee,'pright_knee';...
    pln1,'pln1';pln4,'pln4';pright_foot,'pright_foot';pleft_foot,'pleft_foot'});


% save all the matrices
save('7link_model.mat')
